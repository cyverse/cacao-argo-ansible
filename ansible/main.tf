terraform {
  required_providers {
    openstack-auto-topology = {
      # a special provider for `openstack network auto allocated topology create --or-show`
      source = "zhxu73/openstack-auto-topology"
      version = "0.1.0"
    }
    openstack = {
      source = "terraform-provider-openstack/openstack"
    }
    local = {
      source = "hashicorp/local"
    }
    null = {
      source = "hashicorp/null"
    }
    time = {
      source = "hashicorp/time"
    }
    random = {
      source = "hashicorp/random"
    }
  }
}


