#!/bin/bash

function handle_error()
{
    pwd
    ls -l
    if [[ -f tf.log ]]; then
        echo "============================================================"
        echo "searching for error in logs"
        cat tf.log | grep -v "\[DEBUG\]" | grep -E "Error|error|Err" -B 2 -A 5
    fi
}

set -o pipefail
set -eE
trap handle_error ERR

export TF_HTTP_RETRY_MAX=10
export TF_HTTP_RETRY_WAIT_MIN=5
export TF_HTTP_RETRY_WAIT_MAX=600

cd "{{TF_PROJECT_PATH}}"
terraform init -no-color -lock=true -lock-timeout=120s
terraform refresh -no-color -lock=true 2>&1 | tee tf.log
terraform apply -destroy -no-color -lock=true -auto-approve 2>&1 | tee -a tf.log
