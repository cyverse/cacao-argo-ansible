# cacao-argo-ansible
Argo workflow definition and ansible roles and playbooks for CACAO for provision and deployment.

# Workflows
## `terraform.yml`
Provision resource via Terraform template, return Terraform state (JSON) as output.

# Ansible
## `tf-generic-module` role
Generate a root module and setup given terraform moudle as a child module, and run Terraform operation (e.g. `apply`, `plan`, `destroy`) on it.
