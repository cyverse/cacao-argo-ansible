apiVersion: argoproj.io/v1alpha1
kind: Workflow
metadata:
  generateName: tf-os-
spec:
  ttlStrategy:
    secondsAfterCompletion: 2592000 # 30 days
  onExit: exit-handler
  serviceAccountName: awm-argo
  entrypoint: tf-openstack-dag
  arguments:
    parameters:
    - name: transaction
      value: ""
    - name: deployment-id
      value: ""
    - name: template-type
      value: ""
    - name: template-id
      value: ""
    - name: tf-state-key
      value: ""
    - name: git-url
      value: ""
    - name: git-branch
      value: ""
    - name: git-tag
      value: ""
    - name: git-commit
      value: ""
    - name: sub-path
      value: ""
    - name: username
      value: ""
    - name: k8s-openstack-secrets
      value: ""
    - name: k8s-git-secrets
      value: ""
    - name: ansible-vars-yaml
      value: ""
    - name: provider
      value: ""
    - name: NATS_URL
      value: ""
    - name: NATS_CLUSTER_ID
      value: ""
    - name: NATS_SUBJECT
      value: ""
  volumeClaimTemplates:
  - metadata:
      name: tf-ws-vol
    spec:
      accessModes: [ "ReadWriteOnce" ]
      resources:
        requests:
          storage: 50Mi
  volumes:
  - name: cacao-ssh-key
    secret:
      secretName: cacao-ssh-key
      defaultMode: 0400
  templates:
  ##
  # STEPS DEFINITION HERE
  ##
  - name: tf-openstack-dag
    steps:
      - - name: clone-repo-branch
          template: template-clone-repo-branch
          when: "'{{workflow.parameters.git-branch}}' != ''"
      - - name: clone-repo-tag
          template: template-clone-repo-tag
          when: "'{{workflow.parameters.git-tag}}' != ''"
      - - name: prep-ansible-vars
          template: template-prep-ansible-vars-file
          arguments:
            parameters:
            - name: ansible-vars-yaml
              value: "{{workflow.parameters.ansible-vars-yaml}}"
            - name: tf-workspace
              value: "{{workflow.parameters.tf-state-key}}"
      - - name: tf-apply
          template: template-tf-apply
          arguments:
            parameters:
            - name: tf-workspace
              value: "{{workflow.parameters.tf-state-key}}"
      # - - name: ansible
      #     template: template-ansible
      #     arguments:
      #       parameters:
      #       - name: tf-workspace
      #         value: "{{workflow.parameters.tf-state-key}}"
  - name: exit-handler
    steps:
      - - name: pull-tf-state
          template: template-tf-state-pull
          arguments:
            parameters:
            - name: tf-workspace
              value: "{{workflow.parameters.tf-state-key}}"
          continueOn:
            failed: true
            error: true
      - - name: sends-nats-msg
          template: template-sends-nats-msg

  ##
  # TEMPLATES HERE
  ##
  - name: template-clone-repo-branch
    activeDeadlineSeconds: 600
    retryStrategy:
      limit: "3"
      backoff:
        duration: "5"
        factor: "2"
        maxDuration: "5m"
    container:
      image: registry.gitlab.com/cyverse/cacao-edge-deployment/go-git-cli:latest
      command:
      - /go-git-cli
      - "{{workflow.parameters.git-url}}"
      - /mnt/tf-ws/repo
      - --branch
      - "{{workflow.parameters.git-branch}}"
      volumemounts:
      - name: tf-ws-vol
        mountpath: /mnt/tf-ws
      envFrom:
        - secretRef:
            name: "{{workflow.parameters.k8s-git-secrets}}"
            optional: true
  - name: template-clone-repo-tag
    activeDeadlineSeconds: 600
    retryStrategy:
      limit: "3"
      backoff:
        duration: "5"
        factor: "2"
        maxDuration: "5m"
    container:
      image: registry.gitlab.com/cyverse/cacao-edge-deployment/go-git-cli:latest
      command:
      - /go-git-cli
      - "{{workflow.parameters.git-url}}"
      - /mnt/tf-ws/repo
      - --tag
      - "{{workflow.parameters.git-tag}}"
      volumemounts:
      - name: tf-ws-vol
        mountpath: /mnt/tf-ws
      envFrom:
        - secretRef:
            name: "{{workflow.parameters.k8s-git-secrets}}"
            optional: true

  - name: template-prep-ansible-vars-file
    activeDeadlineSeconds: 600
    inputs:
      parameters:
      - name: ansible-vars-yaml
      - name: tf-workspace
    script:
      image: registry.gitlab.com/cyverse/cacao-argo-ansible/tf-ansible:latest
      command: ["/bin/bash"]
      source: |
          echo "$ANSIBLE_VARS_YAML" > /mnt/tf-ws/ansible-vars.yml

          mkdir -p ~/.ssh
          echo "IdentityFile /mnt/ssh/private_key" > ~/.ssh/config
          chmod g=,o= -R ~/.ssh

          touch config.yml
          ansible-playbook playbook.yml \
              -e @/mnt/tf-ws/ansible-vars.yml \
              -e TF_PROJECT_PATH=/mnt/tf-ws \
              -e TF_WORKSPACE="{{inputs.parameters.tf-workspace}}" \
              -e TF_SUB_PATH="{{workflow.parameters.sub-path}}" \
              -e TF_MODULE="cacao-module"
          ls -l /mnt/tf-ws
          find /mnt/tf-ws
      env:
      - name: ANSIBLE_VARS_YAML
        value: "{{inputs.parameters.ansible-vars-yaml}}"
      volumemounts:
      - name: tf-ws-vol
        mountpath: /mnt/tf-ws

  - name: template-tf-apply
    activeDeadlineSeconds: 7200
    inputs:
      parameters:
      - name: tf-workspace
    script:
      image: registry.gitlab.com/cyverse/cacao-argo-ansible/tf-ansible:latest
      command: ["/bin/bash"]
      source: |
          set -e
          mkdir -p ~/.ssh
          echo "IdentityFile /mnt/ssh/private_key" > ~/.ssh/config
          chmod g=,o= -R ~/.ssh

          eval `ssh-agent`
          ssh-add /mnt/ssh/private_key

          #export TF_CLI_ARGS_apply="-parallelism=1"
          export OS_DEBUG=1
          export TF_LOG=DEBUG
          bash /mnt/tf-ws/terraform.sh
      volumeMounts:
      - name: tf-ws-vol
        mountPath: /mnt/tf-ws
      - name: cacao-ssh-key
        mountPath: "/mnt/ssh"
      envFrom:
        - secretRef:
            name: "{{workflow.parameters.k8s-openstack-secrets}}"
            optional: false

  - name: template-tf-state-pull
    activeDeadlineSeconds: 1200
    retryStrategy:
      limit: "5"
      backoff:
        duration: "5"
        factor: "2"
        maxDuration: "5m"
    inputs:
      parameters:
      - name: tf-workspace
    script:
      image: registry.gitlab.com/cyverse/cacao-argo-ansible/tf-ansible:latest
      command: ["/bin/bash"]
      source: |
          set -e
          cd /mnt/tf-ws
          terraform init -no-color -lock=true -lock-timeout=30s
          terraform state pull | tee /mnt/tf-ws/terraform.tfstate
          ls -l /mnt/tf-ws/terraform.tfstate
      volumeMounts:
      - name: tf-ws-vol
        mountPath: /mnt/tf-ws
      envFrom:
        - secretRef:
            name: "{{workflow.parameters.k8s-openstack-secrets}}"
            optional: false

  # this template was used for exec ansible as a separate step, this is not currently used
  - name: template-ansible
    activeDeadlineSeconds: 1200
    inputs:
      parameters:
      - name: tf-workspace
    script:
      image: registry.gitlab.com/cyverse/cacao-argo-ansible/tf-ansible:latest
      command: ["/bin/bash"]
      source: |
          set -e 
          if [ ! -d "/mnt/tf-ws/modules/cacao-module/ansible" ] 
          then
              echo /mnt/tf-ws/modules/cacao-module/ansible does not exists, skipping ansible...
              exit 0
          fi
          if [ ! -f "/mnt/tf-ws/hosts.yml" ] 
          then
              echo /mnt/tf-ws/hosts.yml does not exists, skipping ansible...
              exit 0
          fi

          cd /mnt/tf-ws/modules/cacao-module/ansible
          if [ -f "requirements.yaml" ] 
          then
              ansible-galaxy install -r requirements.yaml
          fi
          if [ -f "requirements.yml" ] 
          then
              ansible-galaxy install -r requirements.yml
          fi
          ansible-playbook playbook.yaml \
              -i /mnt/tf-ws/hosts.yml \
              --private-key /mnt/ssh/private_key
      volumeMounts:
      - name: tf-ws-vol
        mountPath: /mnt/tf-ws
      - name: cacao-ssh-key
        mountPath: "/mnt/ssh"

  - name: template-sends-nats-msg
    activeDeadlineSeconds: 600
    retryStrategy:
      limit: "5"
      backoff:
        duration: "5"
        factor: "2"
        maxDuration: "5m"
    script:
      image: registry.gitlab.com/cyverse/cacao-edge-deployment/workflow-event-emitter:latest
      command: ["/bin/sh"]
      source: |
          if [ ! -s /mnt/tf-ws/terraform.tfstate ]; then
            echo "/mnt/tf-ws/terraform.tfstate is empty"
            echo "{}" > /mnt/tf-ws/terraform.tfstate
          fi
          workflow-event-emitter pub "{{workflow.name}}" "{{workflow.status}}" \
              --provider "{{workflow.parameters.provider}}" --wf-output /mnt/tf-ws/terraform.tfstate
      env:
      - name: NATS_URL
        value: "{{workflow.parameters.NATS_URL}}"
      - name: NATS_CLIENT_ID
        value: "{{workflow.name}}" # workflow name as client id
      - name: NATS_CLUSTER_ID
        value: "{{workflow.parameters.NATS_CLUSTER_ID}}"
      - name: NATS_SUBJECT
        value: "{{workflow.parameters.NATS_SUBJECT}}"
      - name: METADATA
        value: |
            {"deployment":"{{workflow.parameters.deployment-id}}","template":"{{workflow.parameters.template-id}}",
            "username":"{{workflow.parameters.username}}","transaction":"{{workflow.parameters.transaction}}",
            "template_type":"{{workflow.parameters.template-type}}"}
      volumeMounts:
      - name: tf-ws-vol
        mountPath: /mnt/tf-ws

